<p>Este sistema cria os engines com os m&aacute;ximos e m&iacute;nimos (curvas) baseados no trabalho entregue LINK.</p>
<p>Utiliza somente curvas triangulares, por&eacute;m guardaram uma incrivel semelhan&ccedil;a com o sistema vigente (cadeia condicional), devido &agrave; sua linearidade.</p>
<p>Ap&oacute;s criado o engine personalizado (com m&aacute;ximos e m&iacute;nimos e as curvas correspondentes automaticamente calculadas), basta criar valores na p&aacute;gina seguinte.</p>
<p>Algoritmo desta fase</p>
<ul>
<li>Defini&ccedil;&atilde;o dos valores (todos com 4 decimais)</li>
<li>Salvamento no banco</li>
<li>Cria&ccedil;&atilde;o do c&oacute;digo fonte</li>
<li>Compila&ccedil;&atilde;o em C++ (utilizando a Lib do Fuzzilite)</li>
</ul>