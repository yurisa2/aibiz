<?php
include_once(getabspath("classes/printpage.php"));

function DisplayMasterTableInfoForPrint_engines($params)
{
	global $cman;
	
	$detailtable = $params["detailtable"];
	$keys = $params["keys"];
	
	$xt = new Xtempl();
	
	$tName = "engines";
	$xt->eventsObject = getEventObject($tName);

	$pageType = PAGE_PRINT;

	$mParams  = array();
	$mParams["xt"] = &$xt;
	$mParams["mode"] = PRINT_MASTER;
	$mParams["pageType"] = $pageType;
	$mParams["tName"] = $tName;
	$masterPage = new PrintPage($mParams);
	
	$cipherer = new RunnerCipherer( $tName );
	$settings = new ProjectSettings($tName, $pageType);
	$connection = $cman->byTable( $tName );
	
	$masterQuery = $settings->getSQLQuery();
	$viewControls = new ViewControlsContainer($settings, $pageType, $masterPage);
	
	$where = "";
	$keysAssoc = array();
	$showKeys = "";

	if( $detailtable == "values" )
	{
		$keysAssoc["id_engine"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("id_engine", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("id_engine", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("id_engine", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("engines","id_engine").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}
	
	if( !$where )
		return;
	
	$str = SecuritySQL("Export", $tName );
	if( strlen($str) )
		$where.= " and ".$str;
	
	$strWhere = whereAdd( $masterQuery->m_where->toSql($masterQuery), $where );
	if( strlen($strWhere) )
		$strWhere= " where ".$strWhere." ";
		
	$strSQL = $masterQuery->HeadToSql().' '.$masterQuery->FromToSql().$strWhere.$masterQuery->TailToSql();
	LogInfo($strSQL);
	
	$data = $cipherer->DecryptFetchedArray( $connection->query( $strSQL )->fetchAssoc() );
	if( !$data )
		return;
	
	// reassign pagetitlelabel function adding extra params
	$xt->assign_function("pagetitlelabel", "xt_pagetitlelabel", array("record" => $data, "settings" => $settings));	
	
	$keylink = "";
	$keylink.= "&key1=".runner_htmlspecialchars(rawurlencode(@$data["id_engine"]));
	
	$xt->assign("id_engine_mastervalue", $viewControls->showDBValue("id_engine", $data, $keylink));
	$format = $settings->getViewFormat("id_engine");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("id_engine")))
		$class = ' rnr-field-number';
		
	$xt->assign("id_engine_class", $class); // add class for field header as field value
	$xt->assign("nome_mastervalue", $viewControls->showDBValue("nome", $data, $keylink));
	$format = $settings->getViewFormat("nome");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("nome")))
		$class = ' rnr-field-number';
		
	$xt->assign("nome_class", $class); // add class for field header as field value
	$xt->assign("dolar_max_mastervalue", $viewControls->showDBValue("dolar_max", $data, $keylink));
	$format = $settings->getViewFormat("dolar_max");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("dolar_max")))
		$class = ' rnr-field-number';
		
	$xt->assign("dolar_max_class", $class); // add class for field header as field value
	$xt->assign("dolar_min_mastervalue", $viewControls->showDBValue("dolar_min", $data, $keylink));
	$format = $settings->getViewFormat("dolar_min");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("dolar_min")))
		$class = ' rnr-field-number';
		
	$xt->assign("dolar_min_class", $class); // add class for field header as field value
	$xt->assign("pedido_max_mastervalue", $viewControls->showDBValue("pedido_max", $data, $keylink));
	$format = $settings->getViewFormat("pedido_max");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("pedido_max")))
		$class = ' rnr-field-number';
		
	$xt->assign("pedido_max_class", $class); // add class for field header as field value
	$xt->assign("pedido_min_mastervalue", $viewControls->showDBValue("pedido_min", $data, $keylink));
	$format = $settings->getViewFormat("pedido_min");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("pedido_min")))
		$class = ' rnr-field-number';
		
	$xt->assign("pedido_min_class", $class); // add class for field header as field value
	$xt->assign("obs_mastervalue", $viewControls->showDBValue("obs", $data, $keylink));
	$format = $settings->getViewFormat("obs");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("obs")))
		$class = ' rnr-field-number';
		
	$xt->assign("obs_class", $class); // add class for field header as field value
	$xt->assign("date_mastervalue", $viewControls->showDBValue("date", $data, $keylink));
	$format = $settings->getViewFormat("date");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("date")))
		$class = ' rnr-field-number';
		
	$xt->assign("date_class", $class); // add class for field header as field value
	$xt->assign("timestamp_mastervalue", $viewControls->showDBValue("timestamp", $data, $keylink));
	$format = $settings->getViewFormat("timestamp");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("timestamp")))
		$class = ' rnr-field-number';
		
	$xt->assign("timestamp_class", $class); // add class for field header as field value
	$xt->assign("filename_mastervalue", $viewControls->showDBValue("filename", $data, $keylink));
	$format = $settings->getViewFormat("filename");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("filename")))
		$class = ' rnr-field-number';
		
	$xt->assign("filename_class", $class); // add class for field header as field value
	$xt->assign("sourcecode_link_mastervalue", $viewControls->showDBValue("sourcecode_link", $data, $keylink));
	$format = $settings->getViewFormat("sourcecode_link");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("sourcecode_link")))
		$class = ' rnr-field-number';
		
	$xt->assign("sourcecode_link_class", $class); // add class for field header as field value
	$xt->assign("sourcecode_text_mastervalue", $viewControls->showDBValue("sourcecode_text", $data, $keylink));
	$format = $settings->getViewFormat("sourcecode_text");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("sourcecode_text")))
		$class = ' rnr-field-number';
		
	$xt->assign("sourcecode_text_class", $class); // add class for field header as field value
	$xt->assign("comp_line_mastervalue", $viewControls->showDBValue("comp_line", $data, $keylink));
	$format = $settings->getViewFormat("comp_line");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("comp_line")))
		$class = ' rnr-field-number';
		
	$xt->assign("comp_line_class", $class); // add class for field header as field value

	$layout = GetPageLayout("engines", 'masterprint');
	if( $layout )
		$xt->assign("pageattrs", 'class="'.$layout->style." page-".$layout->name.'"');

	$xt->displayPartial(GetTemplateName("engines", "masterprint"));
}

?>