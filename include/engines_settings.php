<?php
require_once(getabspath("classes/cipherer.php"));




$tdataengines = array();
	$tdataengines[".truncateText"] = true;
	$tdataengines[".NumberOfChars"] = 80;
	$tdataengines[".ShortName"] = "engines";
	$tdataengines[".OwnerID"] = "";
	$tdataengines[".OriginalTable"] = "engines";

//	field labels
$fieldLabelsengines = array();
$fieldToolTipsengines = array();
$pageTitlesengines = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsengines["Portuguese(Brazil)"] = array();
	$fieldToolTipsengines["Portuguese(Brazil)"] = array();
	$pageTitlesengines["Portuguese(Brazil)"] = array();
	$fieldLabelsengines["Portuguese(Brazil)"]["id_engine"] = "Executar";
	$fieldToolTipsengines["Portuguese(Brazil)"]["id_engine"] = "";
	$fieldLabelsengines["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsengines["Portuguese(Brazil)"]["nome"] = "Nome amigavel para o engine";
	$fieldLabelsengines["Portuguese(Brazil)"]["dolar_max"] = "Dolar Max";
	$fieldToolTipsengines["Portuguese(Brazil)"]["dolar_max"] = "Dólar Máximo praticado no período anterior (4 decimais)";
	$fieldLabelsengines["Portuguese(Brazil)"]["dolar_min"] = "Dolar Min";
	$fieldToolTipsengines["Portuguese(Brazil)"]["dolar_min"] = "Dólar Mínimo Praticado no período anterior (4 decimais)";
	$fieldLabelsengines["Portuguese(Brazil)"]["pedido_max"] = "Pedido Max";
	$fieldToolTipsengines["Portuguese(Brazil)"]["pedido_max"] = "\$\$ Máximo de pedidos a serem cumpridos (geralmente seu orcamento máximo)";
	$fieldLabelsengines["Portuguese(Brazil)"]["pedido_min"] = "Pedido Min";
	$fieldToolTipsengines["Portuguese(Brazil)"]["pedido_min"] = "\$\$ Mínimo de pedidos a serem cumpridos (Pedidos vencendo no período)";
	$fieldLabelsengines["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsengines["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsengines["Portuguese(Brazil)"]["date"] = "Data";
	$fieldToolTipsengines["Portuguese(Brazil)"]["date"] = "Data da criação (Automático)";
	$fieldLabelsengines["Portuguese(Brazil)"]["timestamp"] = "Timestamp";
	$fieldToolTipsengines["Portuguese(Brazil)"]["timestamp"] = "";
	$fieldLabelsengines["Portuguese(Brazil)"]["filename"] = "Filename";
	$fieldToolTipsengines["Portuguese(Brazil)"]["filename"] = "";
	$fieldLabelsengines["Portuguese(Brazil)"]["sourcecode_link"] = "Sourcecode Link";
	$fieldToolTipsengines["Portuguese(Brazil)"]["sourcecode_link"] = "";
	$fieldLabelsengines["Portuguese(Brazil)"]["sourcecode_text"] = "Sourcecode Text";
	$fieldToolTipsengines["Portuguese(Brazil)"]["sourcecode_text"] = "";
	$fieldLabelsengines["Portuguese(Brazil)"]["comp_line"] = "Comp Line";
	$fieldToolTipsengines["Portuguese(Brazil)"]["comp_line"] = "";
	if (count($fieldToolTipsengines["Portuguese(Brazil)"]))
		$tdataengines[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsengines[""] = array();
	$fieldToolTipsengines[""] = array();
	$pageTitlesengines[""] = array();
	$fieldLabelsengines[""]["filename"] = "Filename";
	$fieldToolTipsengines[""]["filename"] = "";
	$fieldLabelsengines[""]["sourcecode_link"] = "Sourcecode Link";
	$fieldToolTipsengines[""]["sourcecode_link"] = "";
	$fieldLabelsengines[""]["sourcecode_text"] = "Sourcecode Text";
	$fieldToolTipsengines[""]["sourcecode_text"] = "";
	$fieldLabelsengines[""]["comp_line"] = "Comp Line";
	$fieldToolTipsengines[""]["comp_line"] = "";
	if (count($fieldToolTipsengines[""]))
		$tdataengines[".isUseToolTips"] = true;
}


	$tdataengines[".NCSearch"] = true;



$tdataengines[".shortTableName"] = "engines";
$tdataengines[".nSecOptions"] = 0;
$tdataengines[".recsPerRowList"] = 1;
$tdataengines[".recsPerRowPrint"] = 1;
$tdataengines[".mainTableOwnerID"] = "";
$tdataengines[".moveNext"] = 1;
$tdataengines[".entityType"] = 0;

$tdataengines[".strOriginalTableName"] = "engines";





$tdataengines[".showAddInPopup"] = false;

$tdataengines[".showEditInPopup"] = false;

$tdataengines[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataengines[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataengines[".fieldsForRegister"] = array();

$tdataengines[".listAjax"] = false;

	$tdataengines[".audit"] = false;

	$tdataengines[".locking"] = false;

$tdataengines[".edit"] = true;
$tdataengines[".afterEditAction"] = 1;
$tdataengines[".closePopupAfterEdit"] = 1;
$tdataengines[".afterEditActionDetTable"] = "";

$tdataengines[".add"] = true;
$tdataengines[".afterAddAction"] = 1;
$tdataengines[".closePopupAfterAdd"] = 1;
$tdataengines[".afterAddActionDetTable"] = "";

$tdataengines[".list"] = true;

$tdataengines[".view"] = true;




$tdataengines[".delete"] = true;

$tdataengines[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataengines[".searchSaving"] = false;
//

$tdataengines[".showSearchPanel"] = true;
		$tdataengines[".flexibleSearch"] = true;

$tdataengines[".isUseAjaxSuggest"] = false;

$tdataengines[".rowHighlite"] = true;



$tdataengines[".addPageEvents"] = false;

// use timepicker for search panel
$tdataengines[".isUseTimeForSearch"] = false;



$tdataengines[".badgeColor"] = "7B68EE";


$tdataengines[".allSearchFields"] = array();
$tdataengines[".filterFields"] = array();
$tdataengines[".requiredSearchFields"] = array();



$tdataengines[".googleLikeFields"] = array();
$tdataengines[".googleLikeFields"][] = "filename";
$tdataengines[".googleLikeFields"][] = "sourcecode_link";
$tdataengines[".googleLikeFields"][] = "sourcecode_text";
$tdataengines[".googleLikeFields"][] = "comp_line";


$tdataengines[".advSearchFields"] = array();
$tdataengines[".advSearchFields"][] = "sourcecode_text";
$tdataengines[".advSearchFields"][] = "sourcecode_link";
$tdataengines[".advSearchFields"][] = "filename";
$tdataengines[".advSearchFields"][] = "comp_line";

$tdataengines[".tableType"] = "list";

$tdataengines[".printerPageOrientation"] = 0;
$tdataengines[".nPrinterPageScale"] = 100;

$tdataengines[".nPrinterSplitRecords"] = 40;

$tdataengines[".nPrinterPDFSplitRecords"] = 40;



$tdataengines[".geocodingEnabled"] = false;





$tdataengines[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdataengines[".pageSize"] = 20;

$tdataengines[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataengines[".strOrderBy"] = $tstrOrderBy;

$tdataengines[".orderindexes"] = array();

$tdataengines[".sqlHead"] = "SELECT id_engine,  nome,  dolar_max,  dolar_min,  pedido_max,  pedido_min,  obs,  \"date\",  \"timestamp\",  filename,  sourcecode_link,  sourcecode_text,  comp_line";
$tdataengines[".sqlFrom"] = "FROM engines";
$tdataengines[".sqlWhereExpr"] = "";
$tdataengines[".sqlTail"] = "";



//fill array of tabs for add page
$arrAddTabs = array();
	$tabFields = array();	
	
			
		$tabFields[] = "dolar_min";
			
		$tabFields[] = "dolar_max";
$arrAddTabs[] = array('tabId'=>'Dolar__Intervalo_1',
					  'tabName'=>"Dolar (Intervalo)",
					  'nType'=>'1',
					  'nOrder'=>4,
					  'tabGroup'=>0,
					  'arrFields'=> $tabFields,
					  'expandSec'=>1);
	$tabFields = array();	
	
			
		$tabFields[] = "pedido_min";
			
		$tabFields[] = "pedido_max";
$arrAddTabs[] = array('tabId'=>'Pedidos__Intervalo_1',
					  'tabName'=>"Pedidos (Intervalo)",
					  'nType'=>'1',
					  'nOrder'=>7,
					  'tabGroup'=>0,
					  'arrFields'=> $tabFields,
					  'expandSec'=>1);
$tdataengines[".arrAddTabs"] = $arrAddTabs;








//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataengines[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataengines[".arrGroupsPerPage"] = $arrGPP;


$tableKeysengines = array();
$tableKeysengines[] = "id_engine";
$tdataengines[".Keys"] = $tableKeysengines;

$tdataengines[".listFields"] = array();
$tdataengines[".listFields"][] = "date";
$tdataengines[".listFields"][] = "nome";
$tdataengines[".listFields"][] = "dolar_min";
$tdataengines[".listFields"][] = "dolar_max";
$tdataengines[".listFields"][] = "pedido_min";
$tdataengines[".listFields"][] = "pedido_max";
$tdataengines[".listFields"][] = "sourcecode_text";
$tdataengines[".listFields"][] = "sourcecode_link";
$tdataengines[".listFields"][] = "filename";
$tdataengines[".listFields"][] = "comp_line";
$tdataengines[".listFields"][] = "obs";

$tdataengines[".hideMobileList"] = array();


$tdataengines[".viewFields"] = array();
$tdataengines[".viewFields"][] = "date";
$tdataengines[".viewFields"][] = "nome";
$tdataengines[".viewFields"][] = "dolar_min";
$tdataengines[".viewFields"][] = "dolar_max";
$tdataengines[".viewFields"][] = "pedido_min";
$tdataengines[".viewFields"][] = "pedido_max";
$tdataengines[".viewFields"][] = "sourcecode_text";
$tdataengines[".viewFields"][] = "sourcecode_link";
$tdataengines[".viewFields"][] = "filename";
$tdataengines[".viewFields"][] = "comp_line";
$tdataengines[".viewFields"][] = "obs";

$tdataengines[".addFields"] = array();
$tdataengines[".addFields"][] = "date";
$tdataengines[".addFields"][] = "nome";
$tdataengines[".addFields"][] = "obs";
$tdataengines[".addFields"][] = "dolar_min";
$tdataengines[".addFields"][] = "dolar_max";
$tdataengines[".addFields"][] = "pedido_min";
$tdataengines[".addFields"][] = "pedido_max";

$tdataengines[".masterListFields"] = array();
$tdataengines[".masterListFields"][] = "id_engine";
$tdataengines[".masterListFields"][] = "timestamp";
$tdataengines[".masterListFields"][] = "date";
$tdataengines[".masterListFields"][] = "nome";
$tdataengines[".masterListFields"][] = "dolar_min";
$tdataengines[".masterListFields"][] = "dolar_max";
$tdataengines[".masterListFields"][] = "pedido_min";
$tdataengines[".masterListFields"][] = "pedido_max";
$tdataengines[".masterListFields"][] = "sourcecode_text";
$tdataengines[".masterListFields"][] = "sourcecode_link";
$tdataengines[".masterListFields"][] = "filename";
$tdataengines[".masterListFields"][] = "comp_line";
$tdataengines[".masterListFields"][] = "obs";

$tdataengines[".inlineAddFields"] = array();

$tdataengines[".editFields"] = array();
$tdataengines[".editFields"][] = "date";
$tdataengines[".editFields"][] = "nome";
$tdataengines[".editFields"][] = "dolar_min";
$tdataengines[".editFields"][] = "dolar_max";
$tdataengines[".editFields"][] = "pedido_min";
$tdataengines[".editFields"][] = "pedido_max";
$tdataengines[".editFields"][] = "obs";

$tdataengines[".inlineEditFields"] = array();

$tdataengines[".exportFields"] = array();

$tdataengines[".importFields"] = array();

$tdataengines[".printFields"] = array();

//	id_engine
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_engine";
	$fdata["GoodName"] = "id_engine";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","id_engine");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_engine";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_engine";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["id_engine"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","nome");
	$fdata["FieldType"] = 202;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["nome"] = $fdata;
//	dolar_max
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "dolar_max";
	$fdata["GoodName"] = "dolar_max";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","dolar_max");
	$fdata["FieldType"] = 131;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "dolar_max";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dolar_max";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 4;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["dolar_max"] = $fdata;
//	dolar_min
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "dolar_min";
	$fdata["GoodName"] = "dolar_min";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","dolar_min");
	$fdata["FieldType"] = 131;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "dolar_min";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dolar_min";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 4;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["dolar_min"] = $fdata;
//	pedido_max
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "pedido_max";
	$fdata["GoodName"] = "pedido_max";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","pedido_max");
	$fdata["FieldType"] = 131;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "pedido_max";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pedido_max";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 4;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["pedido_max"] = $fdata;
//	pedido_min
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "pedido_min";
	$fdata["GoodName"] = "pedido_min";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","pedido_min");
	$fdata["FieldType"] = 131;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "pedido_min";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pedido_min";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 4;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["pedido_min"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","obs");
	$fdata["FieldType"] = 202;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["obs"] = $fdata;
//	date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "date";
	$fdata["GoodName"] = "date";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","date");
	$fdata["FieldType"] = 135;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "date";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"date\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["date"] = $fdata;
//	timestamp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "timestamp";
	$fdata["GoodName"] = "timestamp";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","timestamp");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "timestamp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"timestamp\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["timestamp"] = $fdata;
//	filename
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "filename";
	$fdata["GoodName"] = "filename";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","filename");
	$fdata["FieldType"] = 202;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "filename";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "filename";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["filename"] = $fdata;
//	sourcecode_link
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "sourcecode_link";
	$fdata["GoodName"] = "sourcecode_link";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","sourcecode_link");
	$fdata["FieldType"] = 202;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "sourcecode_link";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sourcecode_link";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["sourcecode_link"] = $fdata;
//	sourcecode_text
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "sourcecode_text";
	$fdata["GoodName"] = "sourcecode_text";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","sourcecode_text");
	$fdata["FieldType"] = 202;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "sourcecode_text";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sourcecode_text";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["sourcecode_text"] = $fdata;
//	comp_line
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "comp_line";
	$fdata["GoodName"] = "comp_line";
	$fdata["ownerTable"] = "engines";
	$fdata["Label"] = GetFieldLabel("engines","comp_line");
	$fdata["FieldType"] = 202;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "comp_line";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "comp_line";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataengines["comp_line"] = $fdata;


$tables_data["engines"]=&$tdataengines;
$field_labels["engines"] = &$fieldLabelsengines;
$fieldToolTips["engines"] = &$fieldToolTipsengines;
$page_titles["engines"] = &$pageTitlesengines;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["engines"] = array();
//	values
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="values";
		$detailsParam["dOriginalTable"] = "values";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "values";
	$detailsParam["dCaptionTable"] = GetTableCaption("values");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = 0;
	*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = true;
			$detailsParam["previewOnList"] = "1";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["engines"][$dIndex] = $detailsParam;

	
		$detailsTablesData["engines"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["engines"][$dIndex]["masterKeys"][]="id_engine";

				$detailsTablesData["engines"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["engines"][$dIndex]["detailKeys"][]="engine";

// tables which are master tables for current table (detail)
$masterTablesData["engines"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_engines()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id_engine,  nome,  dolar_max,  dolar_min,  pedido_max,  pedido_min,  obs,  \"date\",  \"timestamp\",  filename,  sourcecode_link,  sourcecode_text,  comp_line";
$proto0["m_strFrom"] = "FROM engines";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id_engine",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto6["m_sql"] = "id_engine";
$proto6["m_srcTableName"] = "engines";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto8["m_sql"] = "nome";
$proto8["m_srcTableName"] = "engines";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "dolar_max",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto10["m_sql"] = "dolar_max";
$proto10["m_srcTableName"] = "engines";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "dolar_min",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto12["m_sql"] = "dolar_min";
$proto12["m_srcTableName"] = "engines";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "pedido_max",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto14["m_sql"] = "pedido_max";
$proto14["m_srcTableName"] = "engines";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "pedido_min",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto16["m_sql"] = "pedido_min";
$proto16["m_srcTableName"] = "engines";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto18["m_sql"] = "obs";
$proto18["m_srcTableName"] = "engines";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "date",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto20["m_sql"] = "\"date\"";
$proto20["m_srcTableName"] = "engines";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "timestamp",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto22["m_sql"] = "\"timestamp\"";
$proto22["m_srcTableName"] = "engines";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "filename",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto24["m_sql"] = "filename";
$proto24["m_srcTableName"] = "engines";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "sourcecode_link",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto26["m_sql"] = "sourcecode_link";
$proto26["m_srcTableName"] = "engines";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "sourcecode_text",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto28["m_sql"] = "sourcecode_text";
$proto28["m_srcTableName"] = "engines";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "comp_line",
	"m_strTable" => "engines",
	"m_srcTableName" => "engines"
));

$proto30["m_sql"] = "comp_line";
$proto30["m_srcTableName"] = "engines";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto32=array();
$proto32["m_link"] = "SQLL_MAIN";
			$proto33=array();
$proto33["m_strName"] = "engines";
$proto33["m_srcTableName"] = "engines";
$proto33["m_columns"] = array();
$proto33["m_columns"][] = "id_engine";
$proto33["m_columns"][] = "nome";
$proto33["m_columns"][] = "dolar_max";
$proto33["m_columns"][] = "dolar_min";
$proto33["m_columns"][] = "pedido_max";
$proto33["m_columns"][] = "pedido_min";
$proto33["m_columns"][] = "obs";
$proto33["m_columns"][] = "date";
$proto33["m_columns"][] = "timestamp";
$proto33["m_columns"][] = "filename";
$proto33["m_columns"][] = "sourcecode_link";
$proto33["m_columns"][] = "sourcecode_text";
$proto33["m_columns"][] = "comp_line";
$obj = new SQLTable($proto33);

$proto32["m_table"] = $obj;
$proto32["m_sql"] = "engines";
$proto32["m_alias"] = "";
$proto32["m_srcTableName"] = "engines";
$proto34=array();
$proto34["m_sql"] = "";
$proto34["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto34["m_column"]=$obj;
$proto34["m_contained"] = array();
$proto34["m_strCase"] = "";
$proto34["m_havingmode"] = false;
$proto34["m_inBrackets"] = false;
$proto34["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto34);

$proto32["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto32);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="engines";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_engines = createSqlQuery_engines();


	
		;

													

$tdataengines[".sqlquery"] = $queryData_engines;

include_once(getabspath("include/engines_events.php"));
$tableEvents["engines"] = new eventclass_engines;
$tdataengines[".hasEvents"] = true;

$query = &$queryData_engines;
$table = "engines";
// here goes EVENT_INIT_TABLE event
include_once 'includes/ops/status_engine.php';

// Place event code here.
// Use "Add Action" button to add code snippets.
;
unset($query);
?>