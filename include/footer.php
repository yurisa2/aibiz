<hr>
<br><br>
<br><br>
<br><br>
<br><br>

<p>Este sistema cria os engines com os m&aacute;ximos e m&iacute;nimos (curvas) baseados no trabalho entregue <a href="investimento.fcl" target=_blank>LINK</a>.</p>
<p>Utiliza somente curvas triangulares, por&eacute;m guardaram uma incrivel semelhan&ccedil;a com o sistema vigente (cadeia condicional), devido &agrave; sua linearidade.</p>
<p>Ap&oacute;s criado o engine personalizado (com m&aacute;ximos e m&iacute;nimos e as curvas correspondentes automaticamente calculadas), basta criar valores na p&aacute;gina "Valores".</p>
<p>Cada Engine pode ter infinitos cálculos</p>
<p>Algoritmo da criação de engines</p>
<ul>
<li>Defini&ccedil;&atilde;o dos valores (todos com 4 decimais)</li>
<li>Salvamento no banco</li>
<li>Cria&ccedil;&atilde;o do c&oacute;digo fonte</li>
<li>Compila&ccedil;&atilde;o em C++ (utilizando a Lib do Fuzzilite)</li>
</ul>

<p>Após criar os engines, proceder para valores e definir valores para os engines ou abrir a abinha laranja que dá para ter um preview</p>


<br><br>
Curvas abaixo:
<br>
<img src="curvas.png" height=70% width=70%>
