<?php
require_once(getabspath("classes/cipherer.php"));




$tdatavalues = array();
	$tdatavalues[".truncateText"] = true;
	$tdatavalues[".NumberOfChars"] = 80;
	$tdatavalues[".ShortName"] = "values";
	$tdatavalues[".OwnerID"] = "";
	$tdatavalues[".OriginalTable"] = "values";

//	field labels
$fieldLabelsvalues = array();
$fieldToolTipsvalues = array();
$pageTitlesvalues = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsvalues["Portuguese(Brazil)"] = array();
	$fieldToolTipsvalues["Portuguese(Brazil)"] = array();
	$pageTitlesvalues["Portuguese(Brazil)"] = array();
	$fieldLabelsvalues["Portuguese(Brazil)"]["id_values"] = "Id Values";
	$fieldToolTipsvalues["Portuguese(Brazil)"]["id_values"] = "";
	$fieldLabelsvalues["Portuguese(Brazil)"]["ValorDolar"] = "Valor Dolar";
	$fieldToolTipsvalues["Portuguese(Brazil)"]["ValorDolar"] = "Valor do Dolar (4 decimais)";
	$fieldLabelsvalues["Portuguese(Brazil)"]["ValorPedidos"] = "Valor Pedidos";
	$fieldToolTipsvalues["Portuguese(Brazil)"]["ValorPedidos"] = "Valor dos pedidos do período";
	$fieldLabelsvalues["Portuguese(Brazil)"]["Armazenamento"] = "Armazenamento";
	$fieldToolTipsvalues["Portuguese(Brazil)"]["Armazenamento"] = "Capacidade de Armazenamento prevista (porcentagem)";
	$fieldLabelsvalues["Portuguese(Brazil)"]["engine"] = "Engine";
	$fieldToolTipsvalues["Portuguese(Brazil)"]["engine"] = "Engine Utilizado para os Cálculos";
	$fieldLabelsvalues["Portuguese(Brazil)"]["resultado"] = "Resultado";
	$fieldToolTipsvalues["Portuguese(Brazil)"]["resultado"] = "";
	if (count($fieldToolTipsvalues["Portuguese(Brazil)"]))
		$tdatavalues[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsvalues[""] = array();
	$fieldToolTipsvalues[""] = array();
	$pageTitlesvalues[""] = array();
	$fieldLabelsvalues[""]["id_values"] = "Id Values";
	$fieldToolTipsvalues[""]["id_values"] = "";
	$fieldLabelsvalues[""]["ValorDolar"] = "Valor Dolar";
	$fieldToolTipsvalues[""]["ValorDolar"] = "";
	$fieldLabelsvalues[""]["ValorPedidos"] = "Valor Pedidos";
	$fieldToolTipsvalues[""]["ValorPedidos"] = "";
	$fieldLabelsvalues[""]["Armazenamento"] = "Armazenamento";
	$fieldToolTipsvalues[""]["Armazenamento"] = "";
	$fieldLabelsvalues[""]["engine"] = "Engine";
	$fieldToolTipsvalues[""]["engine"] = "";
	$fieldLabelsvalues[""]["resultado"] = "Resultado";
	$fieldToolTipsvalues[""]["resultado"] = "";
	if (count($fieldToolTipsvalues[""]))
		$tdatavalues[".isUseToolTips"] = true;
}


	$tdatavalues[".NCSearch"] = true;



$tdatavalues[".shortTableName"] = "values";
$tdatavalues[".nSecOptions"] = 0;
$tdatavalues[".recsPerRowList"] = 1;
$tdatavalues[".recsPerRowPrint"] = 1;
$tdatavalues[".mainTableOwnerID"] = "";
$tdatavalues[".moveNext"] = 1;
$tdatavalues[".entityType"] = 0;

$tdatavalues[".strOriginalTableName"] = "values";





$tdatavalues[".showAddInPopup"] = false;

$tdatavalues[".showEditInPopup"] = false;

$tdatavalues[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatavalues[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatavalues[".fieldsForRegister"] = array();

$tdatavalues[".listAjax"] = false;

	$tdatavalues[".audit"] = false;

	$tdatavalues[".locking"] = false;

$tdatavalues[".edit"] = true;
$tdatavalues[".afterEditAction"] = 1;
$tdatavalues[".closePopupAfterEdit"] = 1;
$tdatavalues[".afterEditActionDetTable"] = "";

$tdatavalues[".add"] = true;
$tdatavalues[".afterAddAction"] = 1;
$tdatavalues[".closePopupAfterAdd"] = 1;
$tdatavalues[".afterAddActionDetTable"] = "";

$tdatavalues[".list"] = true;

$tdatavalues[".view"] = true;




$tdatavalues[".delete"] = true;

$tdatavalues[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatavalues[".searchSaving"] = false;
//

$tdatavalues[".showSearchPanel"] = true;
		$tdatavalues[".flexibleSearch"] = true;

$tdatavalues[".isUseAjaxSuggest"] = false;

$tdatavalues[".rowHighlite"] = true;



$tdatavalues[".addPageEvents"] = false;

// use timepicker for search panel
$tdatavalues[".isUseTimeForSearch"] = false;



$tdatavalues[".badgeColor"] = "e8926f";


$tdatavalues[".allSearchFields"] = array();
$tdatavalues[".filterFields"] = array();
$tdatavalues[".requiredSearchFields"] = array();



$tdatavalues[".googleLikeFields"] = array();
$tdatavalues[".googleLikeFields"][] = "id_values";
$tdatavalues[".googleLikeFields"][] = "ValorDolar";
$tdatavalues[".googleLikeFields"][] = "ValorPedidos";
$tdatavalues[".googleLikeFields"][] = "Armazenamento";
$tdatavalues[".googleLikeFields"][] = "engine";
$tdatavalues[".googleLikeFields"][] = "resultado";


$tdatavalues[".advSearchFields"] = array();
$tdatavalues[".advSearchFields"][] = "id_values";
$tdatavalues[".advSearchFields"][] = "engine";
$tdatavalues[".advSearchFields"][] = "ValorDolar";
$tdatavalues[".advSearchFields"][] = "ValorPedidos";
$tdatavalues[".advSearchFields"][] = "Armazenamento";
$tdatavalues[".advSearchFields"][] = "resultado";

$tdatavalues[".tableType"] = "list";

$tdatavalues[".printerPageOrientation"] = 0;
$tdatavalues[".nPrinterPageScale"] = 100;

$tdatavalues[".nPrinterSplitRecords"] = 40;

$tdatavalues[".nPrinterPDFSplitRecords"] = 40;



$tdatavalues[".geocodingEnabled"] = false;





$tdatavalues[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdatavalues[".pageSize"] = 20;

$tdatavalues[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatavalues[".strOrderBy"] = $tstrOrderBy;

$tdatavalues[".orderindexes"] = array();

$tdatavalues[".sqlHead"] = "SELECT id_values,  ValorDolar,  ValorPedidos,  Armazenamento,  engine,  resultado";
$tdatavalues[".sqlFrom"] = "FROM \"values\"";
$tdatavalues[".sqlWhereExpr"] = "";
$tdatavalues[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatavalues[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatavalues[".arrGroupsPerPage"] = $arrGPP;


$tableKeysvalues = array();
$tableKeysvalues[] = "id_values";
$tdatavalues[".Keys"] = $tableKeysvalues;

$tdatavalues[".listFields"] = array();
$tdatavalues[".listFields"][] = "engine";
$tdatavalues[".listFields"][] = "ValorDolar";
$tdatavalues[".listFields"][] = "ValorPedidos";
$tdatavalues[".listFields"][] = "Armazenamento";
$tdatavalues[".listFields"][] = "resultado";

$tdatavalues[".hideMobileList"] = array();


$tdatavalues[".viewFields"] = array();
$tdatavalues[".viewFields"][] = "engine";
$tdatavalues[".viewFields"][] = "ValorDolar";
$tdatavalues[".viewFields"][] = "ValorPedidos";
$tdatavalues[".viewFields"][] = "Armazenamento";
$tdatavalues[".viewFields"][] = "resultado";

$tdatavalues[".addFields"] = array();
$tdatavalues[".addFields"][] = "engine";
$tdatavalues[".addFields"][] = "ValorDolar";
$tdatavalues[".addFields"][] = "ValorPedidos";
$tdatavalues[".addFields"][] = "Armazenamento";

$tdatavalues[".masterListFields"] = array();
$tdatavalues[".masterListFields"][] = "id_values";
$tdatavalues[".masterListFields"][] = "engine";
$tdatavalues[".masterListFields"][] = "ValorDolar";
$tdatavalues[".masterListFields"][] = "ValorPedidos";
$tdatavalues[".masterListFields"][] = "Armazenamento";
$tdatavalues[".masterListFields"][] = "resultado";

$tdatavalues[".inlineAddFields"] = array();

$tdatavalues[".editFields"] = array();
$tdatavalues[".editFields"][] = "engine";
$tdatavalues[".editFields"][] = "ValorDolar";
$tdatavalues[".editFields"][] = "ValorPedidos";
$tdatavalues[".editFields"][] = "Armazenamento";

$tdatavalues[".inlineEditFields"] = array();

$tdatavalues[".exportFields"] = array();

$tdatavalues[".importFields"] = array();

$tdatavalues[".printFields"] = array();

//	id_values
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_values";
	$fdata["GoodName"] = "id_values";
	$fdata["ownerTable"] = "values";
	$fdata["Label"] = GetFieldLabel("values","id_values");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_values";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_values";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatavalues["id_values"] = $fdata;
//	ValorDolar
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "ValorDolar";
	$fdata["GoodName"] = "ValorDolar";
	$fdata["ownerTable"] = "values";
	$fdata["Label"] = GetFieldLabel("values","ValorDolar");
	$fdata["FieldType"] = 131;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "ValorDolar";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ValorDolar";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 4;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatavalues["ValorDolar"] = $fdata;
//	ValorPedidos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ValorPedidos";
	$fdata["GoodName"] = "ValorPedidos";
	$fdata["ownerTable"] = "values";
	$fdata["Label"] = GetFieldLabel("values","ValorPedidos");
	$fdata["FieldType"] = 131;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "ValorPedidos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ValorPedidos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 4;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatavalues["ValorPedidos"] = $fdata;
//	Armazenamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Armazenamento";
	$fdata["GoodName"] = "Armazenamento";
	$fdata["ownerTable"] = "values";
	$fdata["Label"] = GetFieldLabel("values","Armazenamento");
	$fdata["FieldType"] = 131;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "Armazenamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Armazenamento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 4;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatavalues["Armazenamento"] = $fdata;
//	engine
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "engine";
	$fdata["GoodName"] = "engine";
	$fdata["ownerTable"] = "values";
	$fdata["Label"] = GetFieldLabel("values","engine");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "engine";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "engine";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "engines";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "id_engine";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatavalues["engine"] = $fdata;
//	resultado
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "resultado";
	$fdata["GoodName"] = "resultado";
	$fdata["ownerTable"] = "values";
	$fdata["Label"] = GetFieldLabel("values","resultado");
	$fdata["FieldType"] = 131;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "resultado";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "resultado";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 4;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatavalues["resultado"] = $fdata;


$tables_data["values"]=&$tdatavalues;
$field_labels["values"] = &$fieldLabelsvalues;
$fieldToolTips["values"] = &$fieldToolTipsvalues;
$page_titles["values"] = &$pageTitlesvalues;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["values"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["values"] = array();


	
				$strOriginalDetailsTable="engines";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="engines";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "engines";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "1";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["values"][0] = $masterParams;
				$masterTablesData["values"][0]["masterKeys"] = array();
	$masterTablesData["values"][0]["masterKeys"][]="id_engine";
				$masterTablesData["values"][0]["detailKeys"] = array();
	$masterTablesData["values"][0]["detailKeys"][]="engine";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_values()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id_values,  ValorDolar,  ValorPedidos,  Armazenamento,  engine,  resultado";
$proto0["m_strFrom"] = "FROM \"values\"";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id_values",
	"m_strTable" => "values",
	"m_srcTableName" => "values"
));

$proto6["m_sql"] = "id_values";
$proto6["m_srcTableName"] = "values";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "ValorDolar",
	"m_strTable" => "values",
	"m_srcTableName" => "values"
));

$proto8["m_sql"] = "ValorDolar";
$proto8["m_srcTableName"] = "values";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ValorPedidos",
	"m_strTable" => "values",
	"m_srcTableName" => "values"
));

$proto10["m_sql"] = "ValorPedidos";
$proto10["m_srcTableName"] = "values";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Armazenamento",
	"m_strTable" => "values",
	"m_srcTableName" => "values"
));

$proto12["m_sql"] = "Armazenamento";
$proto12["m_srcTableName"] = "values";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "engine",
	"m_strTable" => "values",
	"m_srcTableName" => "values"
));

$proto14["m_sql"] = "engine";
$proto14["m_srcTableName"] = "values";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "resultado",
	"m_strTable" => "values",
	"m_srcTableName" => "values"
));

$proto16["m_sql"] = "resultado";
$proto16["m_srcTableName"] = "values";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "values";
$proto19["m_srcTableName"] = "values";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "id_values";
$proto19["m_columns"][] = "ValorDolar";
$proto19["m_columns"][] = "ValorPedidos";
$proto19["m_columns"][] = "Armazenamento";
$proto19["m_columns"][] = "engine";
$proto19["m_columns"][] = "resultado";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "\"values\"";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "values";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="values";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_values = createSqlQuery_values();


	
		;

						

$tdatavalues[".sqlquery"] = $queryData_values;

include_once(getabspath("include/values_events.php"));
$tableEvents["values"] = new eventclass_values;
$tdatavalues[".hasEvents"] = true;

?>