//File: INVESTIMENTO.cpp
#include "../../fuzzylite/fl/Headers.h"

int main(int argc, char* argv[]){
    using namespace fl;
    //Code automatically generated with fuzzylite 6.0.

    using namespace fl;

        // if (argc < 3) { // We expect 3 arguments: the program name, the source path and the destination path
        //     std::cerr << "Sintaxe: " << argv[0] << " PARAMETROS" << std::endl;
        //     return 1;
        //   }




fl::Engine* engine = new fl::Engine;
engine->setName("Investimento");

fl::InputVariable* inputVariable1 = new fl::InputVariable;
inputVariable1->setName("DolarSiscomex");
inputVariable1->setRange(3.147, 3.294);

inputVariable1->addTerm(new fl::Triangle("DolarMinimo", 3, 3, 3.5));
inputVariable1->addTerm(new fl::Triangle("DolarIntermediario", 3.25, 3.5, 3.75));
inputVariable1->addTerm(new fl::Triangle("DolarMaximo", 3.5, 4, 4));
engine->addInputVariable(inputVariable1);

fl::InputVariable* inputVariable2 = new fl::InputVariable;
inputVariable2->setName("Pedido");
inputVariable2->setRange(14.000, 26.000);

inputVariable2->addTerm(new fl::Triangle("PedidoBaixo", 5, 5, 5.5));
inputVariable2->addTerm(new fl::Triangle("PedidoMedio", 5.25, 5.5, 5.75));
inputVariable2->addTerm(new fl::Triangle("PedidoAlto", 5.5, 6, 6));
engine->addInputVariable(inputVariable2);

fl::InputVariable* inputVariable3 = new fl::InputVariable;
inputVariable3->setName("DisponibilidadeFisica");
inputVariable3->setRange(0.000, 100.000);

inputVariable3->addTerm(new fl::Triangle("BaixaDisponibilidade", 0.000, 0.000, 50.000));
inputVariable3->addTerm(new fl::Triangle("MediaDisponibilidade", 25.000, 50.000, 75.000));
inputVariable3->addTerm(new fl::Triangle("AltaDisponibilidade", 50.000, 100.000, 100.000));
engine->addInputVariable(inputVariable3);

fl::OutputVariable* outputVariable1 = new fl::OutputVariable;
outputVariable1->setName("Orcamento");
outputVariable1->setRange(24.000, 36.000);
// outputVariable1->setLockOutputRange(false);
outputVariable1->setDefaultValue(fl::nan);
// outputVariable1->setLockValidOutput(false);
outputVariable1->setDefuzzifier(new fl::Centroid(200));
outputVariable1->setAggregation(new Maximum);

outputVariable1->addTerm(new fl::Triangle("IBaixo", 24.000, 24.000, 30.000));
outputVariable1->addTerm(new fl::Triangle("IMedio", 27.000, 30.000, 33.000));
outputVariable1->addTerm(new fl::Triangle("IAlto", 30.000, 36.000, 36.000));
engine->addOutputVariable(outputVariable1);

fl::RuleBlock* mamdani = new fl::RuleBlock;
mamdani->setName("mamdani");
mamdani->setDescription("");
mamdani->setEnabled(true);
mamdani->setConjunction(fl::null);
mamdani->setDisjunction(fl::null);
mamdani->setImplication(new AlgebraicProduct);
mamdani->setActivation(new General);

mamdani->addRule(Rule::parse("if DolarSiscomex is DolarMinimo then Orcamento is IAlto", engine));
mamdani->addRule(Rule::parse("if DolarSiscomex is DolarIntermediario then Orcamento is IMedio", engine));
mamdani->addRule(Rule::parse("if DolarSiscomex is DolarMaximo then Orcamento is IBaixo", engine));
mamdani->addRule(Rule::parse("if Pedido is PedidoBaixo then Orcamento is IBaixo", engine));
mamdani->addRule(Rule::parse("if Pedido is PedidoMedio then Orcamento is IMedio", engine));
mamdani->addRule(Rule::parse("if Pedido is PedidoAlto then Orcamento is IAlto", engine));
mamdani->addRule(Rule::parse("if DisponibilidadeFisica is BaixaDisponibilidade then Orcamento is IBaixo", engine));
mamdani->addRule(Rule::parse("if DisponibilidadeFisica is MediaDisponibilidade then Orcamento is IMedio", engine));
mamdani->addRule(Rule::parse("if DisponibilidadeFisica is AltaDisponibilidade then Orcamento is IAlto", engine));
engine->addRuleBlock(mamdani);

    std::string status;
    if (not engine->isReady(&status))
        throw Exception("[engine error] engine is not ready:n" + status, FL_AT);


        // std::cout << argv[0] << std::endl;
        // std::cout << argv[1] << std::endl;
        // std::cout << argv[2] << std::endl;

double valor1;
double valor2;
double valor3;

valor1 = atof(argv[1]);
valor2 = atof(argv[2]);
valor3 = atof(argv[3]);



        inputVariable1->setValue(valor1);
        inputVariable2->setValue(valor2);
        inputVariable3->setValue(valor3);


        engine->process();

        // std::cout <<   inputVariable1->getValue() << std::endl;
        // std::cout <<   inputVariable2->getValue() << std::endl;
        // std::cout <<   inputVariable3->getValue() << std::endl;

std::cout << outputVariable1->getValue() << std::endl;
}